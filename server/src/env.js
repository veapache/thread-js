import dotenv from 'dotenv';

dotenv.config();

const env = {
  app: {
    port: 3001,
    socketPort: 3002,
    secret: 'secret'
  },
  db: {
    database: process.env.DB_NAME,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT,
    logging: false
  },
  imgur: {
    imgurId: process.env.IMGUR_ID,
    imgurSecret: process.env.IMGUR_SECRET
  }
};

export default env;
