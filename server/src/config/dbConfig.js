/*eslint-disable */

// import env from '../env';

// export const { database, username, password, host, port, dialect, logging } = env.db;
export const { database, username, password, host, port, dialect } = 
{
  database: 'thread',
  username: 'postgres',
  password: 'postgres',
  host: 'localhost',
  port: 5432,
  dialect: 'postgres'
};
